# Directory Watcher

It automatically checks the directory and monitors whether any new files have been added to it.\
If it finds new files and does not change the structure in the directory for some time (default 2 minutes), it sends the notification email to predefined addresses.\
It is a small console application with almost no dependencies.

# Download

Go to **Tags** and download artifacts

# Requirements

- Java 10 (JRE or JDK)

# How to use it

1) Modify the configuration properties file and place it in the same directory with *.jar file
2) Open the command line. Go to directory with *jar file and type: ``java -jar directory-watcher.jar``

# Configuration example for the following screenshots

```properties
dw.root.directory = D:\\__temp
dw.create.delete.time = 60
dw.recursive = true
dw.only.files = true
dw.ignored.files = Thumbs.db

sender.enable = true
sender.smtp.host = your_smtp_server_with_port_465_and_ssl
sender.smtp.port = 465
sender.smtp.protocol = SSL
sender.email = your_email_to_send_notifications
sender.password = password_for_the_notification_email
sender.recipients = emails_of_recipients
sender.isolated.emails = true
sender.custom.root.directory = ftp://some_server/__temp
sender.subject.format = Changes in FTP: {0} new file(s)
```

# Screenshots

### Logging in to the console when making a change to the directory structure, including sending an email
![Console](media/console_out.png)

### Email content coming. File references contain an absolute path
![Email](media/email_out.png)