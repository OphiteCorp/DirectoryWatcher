package cz.ophite.mimic.directorywatcher;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * Hlavní konfigurace aplikace. Načítaná z properties.
 *
 * @author mimic
 */
final class ApplicationConfig {

    // Konstanty

    private static final String P_DW_ROOT_DIRECTORY = "dw.root.directory";
    private static final String P_DW_CREATE_DELETE_TIME = "dw.create.delete.time";
    private static final String P_DW_RECURSIVE = "dw.recursive";
    private static final String P_DW_ONLY_FILES = "dw.only.files";
    private static final String P_DW_IGNORED_FILES = "dw.ignored.files";
    private static final String P_SENDER_ENABLE = "sender.enable";
    private static final String P_SENDER_SMTP_HOST = "sender.smtp.host";
    private static final String P_SENDER_SMTP_PORT = "sender.smtp.port";
    private static final String P_SENDER_SMTP_PROTOCOL = "sender.smtp.protocol";
    private static final String P_SENDER_EMAIL = "sender.email";
    private static final String P_SENDER_PASSWORD = "sender.password";
    private static final String P_SENDER_RECIPIENTS = "sender.recipients";
    private static final String P_SENDER_CUSTOM_ROOT_DIRECTORY = "sender.custom.root.directory";
    private static final String P_SENDER_SUBJECT_FORMAT = "sender.subject.format";
    private static final String P_SENDER_ISOLATED_EMAILS = "sender.isolated.emails";

    // Directory Watcher

    private final String rootDirectory;
    private final int createDeleteTime;
    private final boolean recursive;
    private final boolean onlyFiles;
    private final List<String> ignoredFiles;

    // Sender

    private final boolean senderEnable;
    private final String smtpHost;
    private final int smtpPort;
    private final String smtpProtocol;
    private final String senderEmail;
    private final String senderPassword;
    private final List<String> recipients;
    private final String customRootDirectory;
    private final String subjectFormat;
    private final boolean isolatedEmails;

    /**
     * Vytvoří novou instanci konfigurace.
     *
     * @param properties Načtený properties soubor.
     */
    private ApplicationConfig(Properties properties) {
        // directory watcher
        rootDirectory = properties.get(P_DW_ROOT_DIRECTORY).toString();
        createDeleteTime = Integer.valueOf(properties.get(P_DW_CREATE_DELETE_TIME).toString());
        recursive = Boolean.valueOf(properties.get(P_DW_RECURSIVE).toString());
        onlyFiles = Boolean.valueOf(properties.get(P_DW_ONLY_FILES).toString());

        ignoredFiles = new ArrayList<>();
        var files = properties.get(P_DW_IGNORED_FILES).toString().split("\\|");
        for (var file : files) {
            file = file.trim();
            if (file.length() > 0) {
                ignoredFiles.add(file);
            }
        }

        // sender
        senderEnable = Boolean.valueOf(properties.get(P_SENDER_ENABLE).toString());
        smtpHost = properties.get(P_SENDER_SMTP_HOST).toString();
        smtpPort = Integer.valueOf(properties.get(P_SENDER_SMTP_PORT).toString());
        smtpProtocol = properties.get(P_SENDER_SMTP_PROTOCOL).toString();
        senderEmail = properties.get(P_SENDER_EMAIL).toString();
        senderPassword = properties.get(P_SENDER_PASSWORD).toString();
        recipients = Arrays.asList(properties.get(P_SENDER_RECIPIENTS).toString().split("\\|"));
        customRootDirectory = properties.get(P_SENDER_CUSTOM_ROOT_DIRECTORY).toString();
        subjectFormat = properties.get(P_SENDER_SUBJECT_FORMAT).toString();
        isolatedEmails = Boolean.valueOf(properties.get(P_SENDER_ISOLATED_EMAILS).toString());
    }

    /**
     * Načte konfiguraci z externího properties souboru.
     *
     * @param propertiesFile Externí properties soubor.
     */
    static ApplicationConfig load(File propertiesFile) throws IOException {
        var props = new Properties();
        props.load(new FileInputStream(propertiesFile));
        return new ApplicationConfig(props);
    }

    String getRootDirectory() {
        return rootDirectory;
    }

    int getCreateDeleteTime() {
        return createDeleteTime;
    }

    boolean isRecursive() {
        return recursive;
    }

    boolean isOnlyFiles() {
        return onlyFiles;
    }

    String getSmtpHost() {
        return smtpHost;
    }

    int getSmtpPort() {
        return smtpPort;
    }

    String getSenderEmail() {
        return senderEmail;
    }

    String getSenderPassword() {
        return senderPassword;
    }

    List<String> getRecipients() {
        return recipients;
    }

    String getCustomRootDirectory() {
        return customRootDirectory;
    }

    String getSubjectFormat() {
        return subjectFormat;
    }

    String getSmtpProtocol() {
        return smtpProtocol;
    }

    boolean isIsolatedEmails() {
        return isolatedEmails;
    }

    boolean isSenderEnable() {
        return senderEnable;
    }

    List<String> getIgnoredFiles() {
        return ignoredFiles;
    }
}
