package cz.ophite.mimic.directorywatcher;

import cz.ophite.mimic.directorywatcher.sender.EmailSender;
import cz.ophite.mimic.directorywatcher.sender.SenderConfig;
import cz.ophite.mimic.directorywatcher.watcher.*;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

/**
 * Hlavní třída aplikace.
 *
 * @author mimic
 */
public final class Application {

    private static final String CONFIG_FILE = "directory-watcher.properties";
    private static final SimpleDateFormat SDF = new SimpleDateFormat("dd.MM.YY - HH:mm:ss");
    private final EmailSender sender;

    public static void main(String... args) throws IOException {
        new Application();
    }

    private Application() throws IOException {
        System.out.println("Starting...");

        // načtení konfigurace
        var appConfig = ApplicationConfig.load(new File(CONFIG_FILE));

        // konfigurace pro odesílání emailů
        var senderConfig = new SenderConfig();
        senderConfig.setEnable(appConfig.isSenderEnable());
        senderConfig.setSmtpServer(appConfig.getSmtpHost());
        senderConfig.setSmtpPort(appConfig.getSmtpPort());
        senderConfig.setSmtpProtocol(appConfig.getSmtpProtocol());
        senderConfig.setSenderEmail(appConfig.getSenderEmail());
        senderConfig.setSenderPassword(appConfig.getSenderPassword());
        senderConfig.setRecipients(appConfig.getRecipients());
        senderConfig.setCustomRootDirectory(appConfig.getCustomRootDirectory());
        senderConfig.setSubjectFormat(appConfig.getSubjectFormat());
        senderConfig.setIsolatedEmails(appConfig.isIsolatedEmails());

        // konfigurace pro sledování adresáře
        var dwConfig = new DirectoryWatcherConfig(appConfig.getRootDirectory());
        dwConfig.setRecursive(appConfig.isRecursive());
        dwConfig.setOnlyFiles(appConfig.isOnlyFiles());
        dwConfig.setFireCreateDeleteTime(appConfig.getCreateDeleteTime());
        dwConfig.setIgnoredFiles(appConfig.getIgnoredFiles());

        // vytvoření instance odesílání emailů
        sender = new EmailSender(senderConfig);

        // vytvoření a spuštění instance pro sledování adresáře
        var watcher = new DirectoryWatcher();
        watcher.addListener(new EventHandler());
        watcher.start(dwConfig);
        System.out.printf("Started in '%s' and listening...%n", appConfig.getRootDirectory());
        watcher.waitForFinish();

        System.out.println("Terminated");
    }

    /**
     * Instance handleru pro odchycení událostí pro watcher.
     */
    private final class EventHandler extends DirectoryWatcherAdapter {

        @Override
        public void changeInDirectory(ActionType action, FileEventData data) {
            if (action == ActionType.CREATE || action == ActionType.DELETE) {
                System.out.printf("%s | %s | File: %s%n", SDF.format(new Date()), action,
                        data.getFile().getAbsolutePath());
            }
        }

        @Override
        public void filesAdded(Set<FileEventData> data) {
            System.out.printf("%s | %s files added. Start sending emails...%n", SDF.format(new Date()), data.size());
            sender.sendAddedFiles(data);
        }
    }
}
