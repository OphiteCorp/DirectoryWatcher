package cz.ophite.mimic.directorywatcher.sender;

import cz.ophite.mimic.directorywatcher.watcher.FileEventData;
import org.simplejavamail.email.EmailBuilder;
import org.simplejavamail.email.Recipient;
import org.simplejavamail.mailer.MailerBuilder;
import org.simplejavamail.mailer.config.TransportStrategy;

import javax.mail.Message;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Set;

/**
 * Umožňuje odesílání emailů.
 *
 * @author mimic
 */
public final class EmailSender {

    private final SenderConfig config;

    /**
     * Vytvoří novou instance.
     *
     * @param config Konfigurace pro odesílání emailů.
     */
    public EmailSender(SenderConfig config) {
        this.config = config;
    }

    /**
     * Odešle zprávu/email.
     *
     * @param addedFiles Seznam nových souborů.
     */
    public void sendAddedFiles(Set<FileEventData> addedFiles) {
        if (!config.isEnable()) {
            System.out.printf("Sending emails is off. Otherwise they would be sent to: %s%n", config.getRecipients());
            return;
        }
        // vytvoří mailer pro odeslání emailů
        var mailerBuilder = MailerBuilder
                .withSMTPServer(config.getSmtpServer(), config.getSmtpPort(), config.getSenderEmail(),
                        config.getSenderPassword());
        mailerBuilder.withTransportStrategy(getTransportStrategy());
        mailerBuilder.withSessionTimeout(10 * 1000);
        mailerBuilder.clearEmailAddressCriteria();
        var mailer = mailerBuilder.buildMailer();

        // základní builder pro každý email
        var builder = EmailBuilder.startingBlank();
        builder.from(config.getSenderEmail());
        builder.withSubject(createEmailSubject(addedFiles.size()));
        builder.withHTMLText(createEmailBody(config.getCustomRootDirectory(), addedFiles));

        if (config.isIsolatedEmails()) {
            for (var recipient : config.getRecipients()) {
                recipient = recipient.trim();
                builder.to(new Recipient(recipient, recipient, Message.RecipientType.TO));
                var email = builder.buildEmail();
                mailer.sendMail(email);

                System.out.printf("A notification email has been sent to: %s%n", recipient);
            }
        } else {
            builder.to(createRecipients());
            var email = builder.buildEmail();
            mailer.sendMail(email);

            System.out.println("All email notifications has been sent");
        }
    }

    /**
     * Získá strategii pro připojení k SMTP serveru.
     *
     * @return Strategie.
     */
    private TransportStrategy getTransportStrategy() {
        var protocol = config.getSmtpProtocol().toUpperCase();

        switch (protocol) {
            case "SMTP":
                return TransportStrategy.SMTP;

            case "SSL":
                return TransportStrategy.SMTPS;

            case "TLS":
                return TransportStrategy.SMTP_TLS;
        }
        throw new IllegalStateException("Invalid SMTP protocol type: " + protocol);
    }

    /**
     * Připraví seznam příjemců.
     *
     * @return Seznam příjemců.
     */
    private Recipient[] createRecipients() {
        var recipients = new ArrayList<Recipient>(config.getRecipients().size());

        for (var recipient : config.getRecipients()) {
            recipient = recipient.trim();
            recipients.add(new Recipient(recipient, recipient, Message.RecipientType.TO));
        }
        return recipients.toArray(new Recipient[0]);
    }

    /**
     * Vytvoří předmět emailu.
     *
     * @param filesCount Počet nových souborů.
     *
     * @return Předmět emailu.
     */
    private String createEmailSubject(int filesCount) {
        return MessageFormat.format(config.getSubjectFormat(), filesCount);
    }

    /**
     * Vytvoří tělo emailu.
     *
     * @param rootPath   Hlavní adresář ve kterém se sledují změny.
     * @param addedFiles Seznam nových souborů.
     *
     * @return Tělo emailu.
     */
    private static String createEmailBody(String rootPath, Set<FileEventData> addedFiles) {
        var sb = new StringBuilder();
        sb.append("<html><body style=\"font:normal 13px verdana\">");
        sb.append("<b>List of files added:</b><br><br><ul>");

        for (var file : addedFiles) {
            var fullPath = rootPath + file.getRelativePath();
            var fileLine = MessageFormat
                    .format("<li><a style=\"text-decoration:none\" href=\"{0}\">{1}</a> ({2})</li>", fullPath,
                            file.getRelativePath(), toFileSizeString(file.getFile().length()));
            sb.append(fileLine);
        }
        sb.append(
                "</ul><br><b>This email was generated by the system. Please do not answer him.</b><br>by mimic &copy; 2018");
        sb.append("</body></html>");
        return sb.toString();
    }

    /**
     * Převede velikost souboru v bytech na čitelnýc formát velikosti.
     *
     * @param bytes Velikost v bytech.
     *
     * @return Čitelný formát velikosti souboru.
     */
    private static String toFileSizeString(long bytes) {
        if (bytes < 1024) {
            return bytes + " B";
        }
        var z = (63 - Long.numberOfLeadingZeros(bytes)) / 10;
        return String.format("%.1f %sB", (double) bytes / (1L << (z * 10)), " KMGTPE".charAt(z));
    }
}
