package cz.ophite.mimic.directorywatcher.sender;

import java.util.List;

/**
 * Konfigurace pro odesílání zpráv.
 *
 * @author mimic
 */
public final class SenderConfig {

    private boolean enable;
    private String smtpServer;
    private int smtpPort;
    private String smtpProtocol;
    private String senderEmail;
    private String senderPassword;
    private List<String> recipients;
    private String customRootDirectory;
    private String subjectFormat;
    private boolean isolatedEmails;

    String getSmtpServer() {
        return smtpServer;
    }

    public void setSmtpServer(String smtpServer) {
        this.smtpServer = smtpServer;
    }

    int getSmtpPort() {
        return smtpPort;
    }

    public void setSmtpPort(int smtpPort) {
        this.smtpPort = smtpPort;
    }

    String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

    String getSenderPassword() {
        return senderPassword;
    }

    public void setSenderPassword(String senderPassword) {
        this.senderPassword = senderPassword;
    }

    List<String> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<String> recipients) {
        this.recipients = recipients;
    }

    String getCustomRootDirectory() {
        return customRootDirectory;
    }

    public void setCustomRootDirectory(String customRootDirectory) {
        this.customRootDirectory = customRootDirectory;
    }

    String getSubjectFormat() {
        return subjectFormat;
    }

    public void setSubjectFormat(String subjectFormat) {
        this.subjectFormat = subjectFormat;
    }

    String getSmtpProtocol() {
        return smtpProtocol;
    }

    public void setSmtpProtocol(String smtpProtocol) {
        this.smtpProtocol = smtpProtocol;
    }

    boolean isIsolatedEmails() {
        return isolatedEmails;
    }

    public void setIsolatedEmails(boolean isolatedEmails) {
        this.isolatedEmails = isolatedEmails;
    }

    boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }
}
