package cz.ophite.mimic.directorywatcher.watcher;

import java.io.IOException;

/**
 * Sleduje změny v adresáři.
 *
 * @author mimic
 */
public final class DirectoryWatcher {

    private final DirectoryWatcherHandler handler;
    private Thread thread;

    public DirectoryWatcher() {
        this.handler = new DirectoryWatcherHandler();
    }

    /**
     * Spustí watcher.
     *
     * @param config Konfigurace sledovače.
     *
     * @throws IOException Nastala chyba při nějaké IO operaci.
     */
    public synchronized void start(DirectoryWatcherConfig config) throws IOException {
        if (thread != null && !thread.isInterrupted()) {
            throw new IllegalStateException("Directory watcher already running");
        }
        var process = new DirectoryWatcherProcess(config, handler);
        thread = new Thread(process);
        thread.setName(DirectoryWatcher.class.getSimpleName());
        thread.setPriority(Thread.MIN_PRIORITY);
        thread.setDaemon(true);
        thread.start();
    }

    /**
     * Ukončí sledování adresáře.
     */
    public synchronized void stop() {
        if (thread != null && !thread.isInterrupted()) {
            thread.interrupt();
        }
    }

    /**
     * Počká na kompletní zpracování sledování. V tomto případě bude čakat furt.
     */
    public void waitForFinish() {
        try {
            thread.join();
        } catch (InterruptedException e) {
            // ignorovat
        }
    }

    /**
     * Přidá vlastní listener pro odchycení událostí.
     *
     * @param listener Instance listeneru.
     */
    public void addListener(IDirectoryWatcherListener listener) {
        handler.addListener(listener);
    }
}
