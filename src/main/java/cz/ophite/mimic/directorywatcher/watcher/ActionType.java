package cz.ophite.mimic.directorywatcher.watcher;

/**
 * Typ vykonané akce.
 *
 * @author mimic
 */
public enum ActionType {

    CREATE,
    DELETE,
    MODIFY
}
