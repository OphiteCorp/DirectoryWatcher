package cz.ophite.mimic.directorywatcher.watcher;

import java.io.File;
import java.util.Set;

/**
 * Adapter pro události sledování adresáře.
 *
 * @author mimic
 */
public class DirectoryWatcherAdapter implements IDirectoryWatcherListener {

    @Override
    public void changeInDirectory(ActionType action, FileEventData data) {
    }

    @Override
    public void filesAdded(Set<FileEventData> data) {
    }

    @Override
    public void filesDeleted(Set<File> file) {
    }
}
