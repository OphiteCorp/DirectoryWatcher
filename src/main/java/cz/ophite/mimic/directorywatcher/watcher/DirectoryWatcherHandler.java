package cz.ophite.mimic.directorywatcher.watcher;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Handler pro odchycení událostí nad adresářem.
 *
 * @author mimic
 */
final class DirectoryWatcherHandler {

    private final List<IDirectoryWatcherListener> listeners = new ArrayList<>();

    void addListener(IDirectoryWatcherListener listener) {
        listeners.add(listener);
    }

    /**
     * Nastala nějaká změna v adresáři.
     *
     * @param action Něco bylo přidáno (soubor/adresář).
     * @param data   Informace o souboru/adresáři.
     */
    void fireChangeInDirectory(ActionType action, FileEventData data) {
        for (var listener : listeners) {
            try {
                listener.changeInDirectory(action, data);
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
            }
        }
    }

    /**
     * Byly přidány nějaké soubory/adresáře.
     *
     * @param data Informace o souborech/adresářích.
     */
    void fireFileAdded(Set<FileEventData> data) {
        for (var listener : listeners) {
            try {
                listener.filesAdded(data);
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
            }
        }
    }

    /**
     * Byly odebrány nějaké soubory/adresáře.
     *
     * @param files Cesta k souborům/adresářům.
     */
    void fireFileDeleted(Set<File> files) {
        for (var listener : listeners) {
            try {
                listener.filesDeleted(files);
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
            }
        }
    }
}
