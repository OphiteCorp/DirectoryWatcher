package cz.ophite.mimic.directorywatcher.watcher;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Konfigurace pro sledování adresáře.
 *
 * @author mimic
 */
public final class DirectoryWatcherConfig {

    private final Path rootPath;
    private int fireCreateDeleteTime = 10; // sekund
    private boolean recursive = true;
    private boolean onlyFiles = true;
    private List<String> ignoredFiles = new ArrayList<>(0);

    /**
     * Hlavní adresář pro sledování.
     *
     * @param directory Absolutní cesta do adresáře.
     */
    public DirectoryWatcherConfig(String directory) {
        rootPath = Paths.get(directory);
    }

    /**
     * Vyhodnotí, zda je soubor ignorovaný nebo ne.
     *
     * @param fileName Název souboru.
     *
     * @return True, pokud je ignorovaný.
     */
    public boolean isIgnoredFile(String fileName) {
        for (var file : ignoredFiles) {
            if (file.equalsIgnoreCase(fileName)) {
                return true;
            }
        }
        return false;
    }

    Path getRootPath() {
        return rootPath;
    }

    boolean isRecursive() {
        return recursive;
    }

    public void setRecursive(boolean recursive) {
        this.recursive = recursive;
    }

    int getFireCreateDeleteTime() {
        return fireCreateDeleteTime;
    }

    public void setFireCreateDeleteTime(int fireCreateDeleteTime) {
        this.fireCreateDeleteTime = fireCreateDeleteTime;
    }

    boolean isOnlyFiles() {
        return onlyFiles;
    }

    public void setOnlyFiles(boolean onlyFiles) {
        this.onlyFiles = onlyFiles;
    }

    List<String> getIgnoredFiles() {
        return ignoredFiles;
    }

    public void setIgnoredFiles(List<String> ignoredFiles) {
        this.ignoredFiles = ignoredFiles;
    }
}
