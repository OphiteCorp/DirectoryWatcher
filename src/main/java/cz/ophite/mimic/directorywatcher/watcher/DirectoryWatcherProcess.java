package cz.ophite.mimic.directorywatcher.watcher;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicLong;

import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import static java.nio.file.StandardWatchEventKinds.*;

/**
 * Samotný proces sledování adresáře.
 *
 * @author mimic
 */
final class DirectoryWatcherProcess implements Runnable {

    private static final long MAX_ACTION_FILE_TIME = 60 * 60 * 1000; // ms (1 hodina)

    private final DirectoryWatcherHandler handler;
    private final WatchService watcher;
    private final Map<WatchKey, Path> keys;
    private final Map<ActionType, List<ActionFile>> actionMap;
    private final DirectoryWatcherConfig config;
    private final Object actionMapLock = new Object();
    private volatile AtomicLong actionMapCounter;

    /**
     * Vytvoří novou instanci.
     *
     * @param config  Konfigurace sledování.
     * @param handler Instance handleru pro odchycení událostí.
     *
     * @throws IOException Nastala neočekávaná chyba při IO operaci.
     */
    DirectoryWatcherProcess(DirectoryWatcherConfig config, DirectoryWatcherHandler handler) throws IOException {
        this.watcher = FileSystems.getDefault().newWatchService();
        this.config = config;
        this.keys = new HashMap<>();
        this.handler = handler;
        this.actionMap = new ConcurrentHashMap<>();
        this.actionMapCounter = new AtomicLong();

        if (config.isRecursive()) {
            registerAll(config.getRootPath());
        } else {
            register(config.getRootPath());
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void run() {
        var actionThread = new ActionThread();
        actionThread.start();

        while (!Thread.currentThread().isInterrupted()) {

            WatchKey key;
            try {
                // pokud nastala nějaká změna nad adresářem, tak získá "key", jinak čeká
                key = watcher.take();
            } catch (InterruptedException x) {
                return;
            }

            // pokud je "key" již zaregistrovany mezi klíči, tak ho přeskočí
            var dir = keys.get(key);
            if (dir == null) {
                continue;
            }

            // projde všechny události nad klíčem
            for (var event : key.pollEvents()) {
                var kind = event.kind();

                // pokud byl klíč již smazaný nebo není z nějakého důvodu dostupný, tak událost preskočí
                if (kind == OVERFLOW) {
                    continue;
                }
                var watchEvent = (WatchEvent<Path>) event;
                var name = watchEvent.context();
                var path = dir.resolve(name);
                var data = toFileEventData(path, kind);
                var isFile = !Files.isDirectory(path);

                // pokud je zapnuto rekurzvní sledování a jedná se o vytvoření souboru/adresáře, tak ho též
                // zaregistrujeme, aby bylo možné odchytit události i nad ním
                if (config.isRecursive() && (kind == ENTRY_CREATE)) {
                    try {
                        if (Files.isDirectory(path, NOFOLLOW_LINKS)) {
                            registerAll(path);
                        }
                    } catch (IOException e) {
                        // ignorovat
                    }
                }
                if (!config.isOnlyFiles() || isFile) {
                    if (kind == ENTRY_CREATE) {
                        putNewFileAction(ActionType.CREATE, data);
                        handler.fireChangeInDirectory(ActionType.CREATE, data);

                    } else if (kind == ENTRY_DELETE) {
                        putNewFileAction(ActionType.DELETE, data);
                        handler.fireChangeInDirectory(ActionType.DELETE, data);

                    } else if (kind == ENTRY_MODIFY) {
                        handler.fireChangeInDirectory(ActionType.MODIFY, data);
                    }
                }
            }

            // pokud klíč již není dále k dipozici, tak ho uvolníme z klíčů - zrušíme registraci klíče
            var valid = key.reset();
            if (!valid) {
                keys.remove(key);

                if (keys.isEmpty()) {
                    break;
                }
            }
        }
        actionThread.interrupt();
    }

    /**
     * Zaregistruje adresár do sledování na všechny možné události.
     *
     * @param rootDir Cesta k adresáři.
     *
     * @throws IOException Nastala chyba (např. při čtení adresáře).
     */
    private void register(Path rootDir) throws IOException {
        var key = rootDir.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
        keys.put(key, rootDir);
    }

    /**
     * Zaregistruje adresář a všechny jedho podadresáře.
     *
     * @param rootDir Cesta k adresáři.
     *
     * @throws IOException Nastala chyba (např. při čtení adresáře).
     */
    private void registerAll(Path rootDir) throws IOException {
        Files.walkFileTree(rootDir, new SimpleFileVisitor<>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                register(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    /**
     * Vloží novou akci do mapy akcí.
     *
     * @param actionType Typ akce.
     * @param data       Informace o souboru nebo adresáři.
     */
    private void putNewFileAction(ActionType actionType, FileEventData data) {
        // je potřeba zamknout, aby nebylo možné měnit mapu během vyhodnocení a odesílání emailů
        synchronized (actionMapLock) {
            var list = actionMap.computeIfAbsent(actionType, k -> new CopyOnWriteArrayList<>());
            var actionFile = new ActionFile(data);
            list.add(actionFile);
            actionMapCounter.set(0);
        }
    }

    /**
     * Převede cestu k souboru na informace o události.
     *
     * @param path Cesta k souboru/adresáři.
     * @param kind Druh události.
     *
     * @return Nová instance {@link FileEventData}.
     */
    private FileEventData toFileEventData(Path path, WatchEvent.Kind<?> kind) {
        var data = new FileEventData(config.getRootPath(), path.toFile());

        try {
            if (kind != ENTRY_DELETE) {
                var attrs = Files.readAttributes(path, BasicFileAttributes.class);
                data.setAttrs(attrs);
            }
        } catch (IOException e) {
            // ignorovat
        }
        return data;
    }

    /**
     * Odebere staré soubory, které se již nepoužijou nebo se zasekly.
     */
    private void cleanActionFilesMap() {
        final var now = System.currentTimeMillis();

        for (var entry : actionMap.entrySet()) {
            for (var actionFile : entry.getValue()) {
                if (actionFile.createdTime + MAX_ACTION_FILE_TIME < now) {
                    System.out.printf("Delete the old file '%s' from the cache%n", actionFile.data.getFile().getName());
                    entry.getValue().remove(actionFile);
                }
            }
        }
    }

    /**
     * Zkontroluje, zda je soubor kompletně k dispozici (např. není stále kompletně nakopírovaný).
     *
     * @param file Soubor ke kontrole.
     *
     * @return True, pokud je soubor plně k dispozici.
     */
    private static boolean isCompletelyWritten(File file) {
        try (var in = new FileInputStream(file)) {
            in.read();
            return true;

        } catch (Exception e) {
            return false;
        }
    }

    private final class ActionThread extends Thread {

        private ActionThread() {
            setDaemon(true);
            setName(this.getClass().getSimpleName());
            setPriority(Thread.MIN_PRIORITY);
        }

        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    Thread.sleep(1000);
                    actionMapCounter.incrementAndGet();

                    // má smysl udělat update?
                    if (actionMapCounter.get() > config.getFireCreateDeleteTime()) {
                        // je potřeba zamknout, aby nebylo možné měnit mapu během vyhodnocení a odesílání emailů
                        synchronized (actionMapLock) {
                            if (!actionMap.isEmpty()) {
                                var deletedFiles = new ArrayList<String>();
                                var createdFiles = new ArrayList<String>();

                                // první smyčka pouze vytvoří seznam vytvořených a smazaných souborů/adresářů
                                for (var entry : actionMap.entrySet()) {
                                    if (entry.getKey() == ActionType.DELETE) {
                                        for (var actionFile : entry.getValue()) {
                                            deletedFiles.add(actionFile.data.getFile().getAbsolutePath());
                                        }
                                    } else if (entry.getKey() == ActionType.CREATE) {
                                        for (var actionFile : entry.getValue()) {
                                            createdFiles.add(actionFile.data.getFile().getAbsolutePath());
                                        }
                                    }
                                }
                                if (!deletedFiles.isEmpty() || !createdFiles.isEmpty()) {
                                    System.out.printf("Deleted: %s, Created: %s%n", deletedFiles.size(),
                                            createdFiles.size());
                                }
                                // provolá události pro vytvořený/smazaý soubory/adresáře
                                for (var entry : actionMap.entrySet()) {
                                    if (entry.getKey() == ActionType.DELETE) {
                                        var deleteList = entry.getValue();
                                        var outputList = new HashSet<File>();

                                        for (var actionFile : deleteList) {
                                            if (actionFile.data.getFile().exists() &&
                                                !isCompletelyWritten(actionFile.data.getFile())) {
                                                System.out
                                                        .printf("Delete: The file '%s' is not fully available and will be skipped%n",
                                                                actionFile.data.getFile().getAbsolutePath());
                                                continue;
                                            }
                                            // pokud soubor nepatri mezi nove vytvoreny v poslední smyčce
                                            if (!createdFiles.contains(actionFile.data.getFile().getAbsolutePath())) {
                                                outputList.add(actionFile.data.getFile());
                                            }
                                            deleteList.remove(actionFile);
                                        }
                                        if (!outputList.isEmpty()) {
                                            handler.fireFileDeleted(outputList);
                                        }
                                    } else if (entry.getKey() == ActionType.CREATE) {
                                        var createList = entry.getValue();
                                        var outputList = new HashSet<FileEventData>();

                                        for (var actionFile : createList) {
                                            if (actionFile.data.getFile().exists() &&
                                                !isCompletelyWritten(actionFile.data.getFile())) {
                                                System.out
                                                        .printf("Create: The file '%s' is not fully available and will be skipped%n",
                                                                actionFile.data.getFile().getAbsolutePath());
                                                continue;
                                            }
                                            // pokud soubor neni smazaný v poslední smyčce a skutečně existuje
                                            if (!deletedFiles.contains(actionFile.data.getFile().getAbsolutePath()) &&
                                                actionFile.data.getFile().exists()) {

                                                // velikost souboru nesmí být 0 a soubor nebude mezi ignorovanýma
                                                if (!config.isIgnoredFile(actionFile.data.getFile().getName()) &&
                                                    actionFile.data.getFile().length() > 0) {

                                                    outputList.add(actionFile.data);
                                                }
                                            }
                                            createList.remove(actionFile);
                                        }
                                        if (!outputList.isEmpty()) {
                                            handler.fireFileAdded(outputList);
                                        }
                                    }
                                    // pouze pro rychlejsi uvolnění paměti
                                    deletedFiles.clear();
                                    createdFiles.clear();
                                }
                            }
                            actionMapCounter.set(0);
                        }
                    }
                    cleanActionFilesMap();

                } catch (InterruptedException e) {
                    break;
                }
                // System.out.printf("Waiting tick: %s/%s%n", actionMapCounter, config.getFireCreateDeleteTime());
            }
        }
    }

    private final static class ActionFile {

        private final String uid;
        private final FileEventData data;
        private final long createdTime;

        private ActionFile(FileEventData data) {
            this.data = data;
            this.uid = UUID.randomUUID().toString();
            createdTime = System.currentTimeMillis();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            ActionFile that = (ActionFile) o;
            return Objects.equals(uid, that.uid);
        }

        @Override
        public int hashCode() {
            return Objects.hash(uid);
        }
    }
}