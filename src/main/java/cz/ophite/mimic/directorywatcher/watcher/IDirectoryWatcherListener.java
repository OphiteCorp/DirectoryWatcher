package cz.ophite.mimic.directorywatcher.watcher;

import java.io.File;
import java.util.Set;

/**
 * Události při události nad adresářem.
 *
 * @author mimic
 */
public interface IDirectoryWatcherListener {

    /**
     * Nastala nějaká změna v adresáři.
     *
     * @param action Něco bylo přidáno (soubor/adresář).
     * @param data   Informace o souboru/adresáři.
     */
    void changeInDirectory(ActionType action, FileEventData data);

    /**
     * Byly přidány nějaké soubory/adresáře.
     *
     * @param data Informace o souborech/adresářích.
     */
    void filesAdded(Set<FileEventData> data);

    /**
     * Byly odebrány nějaké soubory/adresáře.
     *
     * @param files Cesta k souborům/adresářům.
     */
    void filesDeleted(Set<File> files);
}
