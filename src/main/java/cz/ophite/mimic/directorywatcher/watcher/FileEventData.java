package cz.ophite.mimic.directorywatcher.watcher;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Objects;

/**
 * Informace o souboru.
 *
 * @author mimic
 */
public final class FileEventData {

    private final File file;
    private final String relativePath;
    private BasicFileAttributes attrs;

    FileEventData(Path rootPath, File file) {
        this.relativePath = calculateRelativePath(rootPath, file);
        this.file = file;
    }

    public File getFile() {
        return file;
    }

    /**
     * Vrací atributy souboru. Může vracet null, pokud atributy nebylo možné získat.
     *
     * @return Atributy souboru/adresáře.
     */
    public BasicFileAttributes getAttrs() {
        return attrs;
    }

    void setAttrs(BasicFileAttributes attrs) {
        this.attrs = attrs;
    }

    /**
     * Získá relativní cestu k souboru.
     *
     * @return Relativní cesta k souboru.
     */
    public String getRelativePath() {
        return relativePath;
    }

    /**
     * Vypočte relativní cestu k souboru.
     *
     * @return Relativní cesta k souboru.
     */
    private static String calculateRelativePath(Path rootPath, File file) {
        var rootUri = rootPath.toFile().toURI();
        var fileUri = file.toURI();
        return rootUri.relativize(fileUri).getPath();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        var that = (FileEventData) o;
        return Objects.equals(relativePath, that.relativePath);
    }

    @Override
    public int hashCode() {
        return Objects.hash(relativePath);
    }
}
